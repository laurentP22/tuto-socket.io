var express = require('express');
var app = express();
var server = require('http').createServer(app)
var io = require('socket.io').listen(server)

users = []
connections = []
rooms = [];

server.listen(process.env.PORT || 3000,'0.0.0.0')

console.log('Server running...')

app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html')
})

io.on('connection',function(socket){
    connections.push(socket)
    console.log('Connected : %s sockets connected',connections.length)

    // Disconnect
    socket.on('disconnect',function(data){
        connections.splice(connections.indexOf(socket),1)   // Remove the connection
        console.log('Disconnected : %s sockets connected',connections.length)
        //if(!socket.userName) return;
        const user = users.filter(user => user.name === socket.userName)[0]
        users.splice(users.indexOf(user),1)   // Remove the user
        updateUserNames()
    })

    // Send Message
    socket.on('send message',function(data){
        io.to(socket.room).emit('new message',{msg: data, user : socket.userName});
    })

    // New User
    socket.on('new user',function(data,room, callback){
        socket.join(room);
        const found = rooms.some(r => r === room)
        if(!found){
            rooms.push(room)
        }
        callback(true);
        socket.userName = data;
        socket.room = room;

        users.push({
            name : socket.userName,
            room : socket.room,
        });

        updateUserNames();
    })

    function updateUserNames(){
        listUsers =  users.filter(user => user.room === socket.room)
        io.to(socket.room).emit('get users',listUsers);
    }
})
